extends Node

var _sound_player = AudioStreamPlayer.new()
var _button_clicked_sound = preload('res://assets/sounds/click.wav')
var _volume_db = Constant.SOUND_VOLUME_DEFAULT
var _along_sound_player = AudioStreamPlayer.new()

func _ready():
	_sound_player.stream = _button_clicked_sound
	_sound_player.autoplay = false
	add_child(_sound_player)
	add_child(_along_sound_player)

func play(name = null, volume_db = Constant.SOUND_VOLUME_DEFAULT):
	if _sound_player.playing || _sound_player.volume_db == Constant.SOUND_VOLUME_MIN:
		return
		
	if name == null or name == '':
		_sound_player.stream = _button_clicked_sound
		_sound_player.volume_db = _volume_db
		_sound_player.play()
	else:
		_sound_player.stream = load('res://assets/sounds/' + name)
		_sound_player.volume_db = volume_db
		_sound_player.play()

func change_volume(value):
	_sound_player.volume_db = value
	_volume_db = value

func stop():
	_sound_player.stop()

func play_second(name):
	if _volume_db == Constant.SOUND_VOLUME_MIN:
		return

	_along_sound_player.stream = load('res://assets/sounds/' + name)
	_along_sound_player.volume_db = _volume_db
	_along_sound_player.play()

func stop_second():
	_along_sound_player.stop()
