extends Node

var data = {
	'levels': [
		{
			'level': 0,
			'name': 'Level 1',
			'initial': 3,
			'steps': [
				{
					'display': '+5',
					'value': '+5'
				},
			],
			'result': 8
		},
		{
			'level': 1,
			'name': 'Level 2',
			'initial': 7,
			'steps': [
				{
					'display': '+19',
					'value': '+19'
				},
				{
					'display': '+23',
					'value': '+23'
				},
			],
			'result': 49
		},
		{
			'level': 2,
			'name': 'Level 3',
			'initial': 17,
			'steps': [
				{
					'display': '+26',
					'value': '+26'
				},
				{
					'display': '+32',
					'value': '+32'
				},
				{
					'display': 'x2',
					'value': '*2'
				},
			],
			'result': 150
		},
		{
			'level': 3,
			'name': 'Level 4',
			'initial': 3,
			'steps': [
				{
					'display': '+5',
					'value': '+5'
				},
				{
					'display': '+9',
					'value': '+9'
				},
				{
					'display': '+16',
					'value': '+16'
				},
				{
					'display': '+29',
					'value': '+29'
				}
			],
			'result': 62
		},
		{
			'level': 4,
			'name': 'Level 5',
			'initial': 3,
			'steps': [
				{
					'display': '+8',
					'value': '+8'
				},
				{
					'display': 'x2',
					'value': '*2'
				},
				{
					'display': '+19',
					'value': '+19'
				},
				{
					'display': '-5',
					'value': '-5'
				}
			],
			'result': 66
		},
		{
			'level': 5,
			'name': 'Level 6',
			'initial': 13,
			'steps': [
				{
					'display': '+19',
					'value': '+19'
				},
				{
					'display': '+25',
					'value': '+25'
				},
				{
					'display': '+32',
					'value': '+32'
				},
				{
					'display': '+23',
					'value': '+23'
				}
			],
			'result': 112
		},
		{
			'level': 6,
			'name': 'Level 7',
			'initial': 28,
			'steps': [
				{
					'display': '-3',
					'value': '-3'
				},
				{
					'display': 'x4',
					'value': '*4'
				},
				{
					'display': '+20',
					'value': '+20'
				},
				{
					'display': '-23',
					'value': '-23'
				}
			],
			'result': 97
		},
		{
			'level': 7,
			'name': 'Level 8',
			'initial': 16,
			'steps': [
				{
					'display': 'x4',
					'value': '*4'
				},
				{
					'display': '+25',
					'value': '+25'
				},
				{
					'display': '-31',
					'value': '-31'
				},
				{
					'display': '+65',
					'value': '+65'
				}
			],
			'result': 123
		},
		{
			'level': 8,
			'name': 'Level 9',
			'initial': 25,
			'steps': [
				{
					'display': '+18',
					'value': '+18'
				},
				{
					'display': '+-13',
					'value': '+-13'
				},
				{
					'display': '--12',
					'value': '--12'
				},
				{
					'display': '+25',
					'value': '+25'
				},
				{
					'display': '--33',
					'value': '--33'
				},
				{
					'display': '+30',
					'value': '+30'
				}
			],
			'result': 130
		},
		{
			'level': 9,
			'name': 'Level 10',
			'initial': 35,
			'steps': [
				{
					'display': '+17',
					'value': '+17'
				},
				{
					'display': 'x2',
					'value': '*2'
				},
				{
					'display': '-19',
					'value': '-19'
				},
				{
					'display': '+25',
					'value': '+25'
				},
				{
					'display': '/2',
					'value': '/2'
				},
				{
					'display': '+26',
					'value': '+26'
				}
			],
			'result': 81
		},
		{
			'level': 10,
			'name': 'Level 11',
			'initial': 13,
			'steps': [
				{
					'display': 'x2',
					'value': '*2'
				},
				{
					'display': '+18',
					'value': '+18'
				},
				{
					'display': '-8',
					'value': '-8'
				},
				{
					'display': '/2',
					'value': '/2'
				},
				{
					'display': '-20',
					'value': '-20'
				},
				{
					'display': '+25',
					'value': '+25'
				}
			],
			'result': 18
		},
		{
			'level': 11,
			'name': 'Level 12',
			'initial': 45,
			'steps': [
				{
					'display': 'x3',
					'value': '*3'
				},
				{
					'display': '+28',
					'value': '+28'
				},
				{
					'display': '-29',
					'value': '-29'
				},
				{
					'display': '/3',
					'value': '/3'
				},
				{
					'display': '+59',
					'value': '+59'
				},
				{
					'display': 'x2',
					'value': '*2'
				}
			],
			'result': 194
		},
		{
			'level': 12,
			'name': 'Level 13',
			'initial': 17,
			'steps': [
				{
					'display': 'x3',
					'value': '*3'
				},
				{
					'display': 'x2',
					'value': '*2'
				},
				{
					'display': '-30',
					'value': '-30'
				},
				{
					'display': '/3',
					'value': '/3'
				},
				{
					'display': 'x4',
					'value': '*4'
				},
				{
					'display': '-25',
					'value': '-25'
				},
				{
					'display': 'x4',
					'value': '*4'
				}
			],
			'result': 284
		},
		{
			'level': 13,
			'name': 'Level 14',
			'initial': 17,
			'steps': [
				{
					'display': 'x-2',
					'value': '*-2'
				},
				{
					'display': '-14',
					'value': '-14'
				},
				{
					'display': '-24',
					'value': '-24'
				},
				{
					'display': '/3',
					'value': '/3'
				},
				{
					'display': '--54',
					'value': '--54'
				},
				{
					'display': '+16',
					'value': '+16'
				},
				{
					'display': '/2',
					'value': '/2'
				}
			],
			'result': 23
		},
		{
			'level': 14,
			'name': 'Level 15',
			'initial': 2,
			'steps': [
				{
					'display': '^3',
					'value': 'pow(base, 3)'
				},
				{
					'display': '-5',
					'value': '-5'
				},
				{
					'display': '^3',
					'value': 'pow(base, 3)'
				},
				{
					'display': '+54',
					'value': '+54'
				},
				{
					'display': '/3',
					'value': '/3'
				},
				{
					'display': 'x4',
					'value': '*4'
				},
				{
					'display': '-16',
					'value': '-16'
				}
			],
			'result': 92
		}
	]
}

var difficulties = ['Normal', 'Medium', 'Hard', 'Very Hard', 'Legendary']
var _rng = RandomNumberGenerator.new()
var _client = HTTPRequest.new()
const LEVEL_ENDPOINT = 'https://runnercounter.escape30.com/levels.json'

func _init():
	var saved_levels = Storage.get_item(Constant.STORAGE_LEVELS_KEY)
	if saved_levels != null && data['levels'].size() < saved_levels.size():
		data['levels'] = saved_levels

func _ready():
	_rng.randomize()
	LPN.cancel(Constant.PN_TAG_NEW_LEVEL)
	
	add_child(_client)
	_client.connect('request_completed', self, '_on_request_completed')
	_client.request(LEVEL_ENDPOINT)
	
func _on_request_completed(_result, status, _headers, body):
	if status == 200:
		var changed = false
		var json = JSON.parse(body.get_string_from_utf8()).result
		var levels = json.get('levels')
		for level in levels:
			var existing_level = get_level(int(level.get('level')))
			if existing_level == null:
				data.get('levels').append(level)
				changed = true
		if changed:
			Storage.set_item(Constant.STORAGE_LEVELS_KEY, data.get('levels'))
			LPN.cancel(Constant.PN_TAG_NEW_LEVEL)
			LPN.show('Inspection: ' + difficulties[_rng.randi_range(0, 4)] + '!  ', 'New levels downloaded.', 14400, Constant.PN_TAG_NEW_LEVEL)

func get_level(level_index):
	for level in data.get('levels'):
		if level.get('level') == level_index:
			return level
	return null

func has_more():
	var levels = Storage.get_item(Constant.STORAGE_LEVELS_KEY)
	levels = data.get('levels') if levels == null else levels
	
	var current_level = Storage.get_item(Constant.STORAGE_CURRENT_LEVEL_KEY)
	current_level = 0 if current_level == null else current_level
	
	return current_level + 1 <= levels.size()
