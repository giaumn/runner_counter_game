extends Node

var _background_music = preload('res://assets/musics/romantic-hip-hop-groove.mp3')
var _music_player
var _started = false
var _value

func _ready():
	_music_player = AudioStreamPlayer.new()
	_music_player.autoplay = false
	_music_player.stream = _background_music
	_music_player.volume_db = Constant.MUSIC_VOLUME_DEFAULT
	add_child(_music_player)

func start():
	if _music_player.playing or _music_player.volume_db == Constant.MUSIC_VOLUME_MIN:
		return
	_started = true
	_music_player.play()

func change_volume(value):
	_value = value
	_music_player.volume_db = _value
	if _value == Constant.MUSIC_VOLUME_MIN:
		_music_player.stop()
	else:
		start()	

func change(name):
	_music_player.stream = load('res://assets/musics/' + name)
	change_volume(_value)

func stop():
	_started = false
	_music_player.stop()
