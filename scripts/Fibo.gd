extends Node

var numbers = [
	1,
	2,
	3,
	5,
	8,
	13,
	21,
	34,
	55,
	89,
	144,
	233,
	377,
	610,
	987,
	1597,
	2584,
	4181,
	6765,
	10946,
	17711,
	28657,
	46368,
	75025
]

func _ready():
	pass

func generate(n: int):
	if n == 0:
		return 1
	
	if n == 1:
		return 2
		
	return generate(n - 1) + generate(n - 2)
