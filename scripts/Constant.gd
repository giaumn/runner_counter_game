extends Node

var OS_IOS = 'IOS'
var OS_ANDROID = 'Android'

var MUSIC_VOLUME_MIN = -30
var MUSIC_VOLUME_MAX = -10
var MUSIC_VOLUME_DEFAULT = -15

var SOUND_VOLUME_MIN = -20
var SOUND_VOLUME_MAX = 0
var SOUND_VOLUME_DEFAULT = -5

var STORAGE_HIGH_SCORE_KEY = 'high_score'
var STORAGE_SOUND_KEY = 'sound'
var STORAGE_VIBRATE_KEY = 'vibrate'
var STORAGE_CURRENT_LEVEL_KEY = 'current_level'
var STORAGE_SCENE_KEY = 'scene'
var STORAGE_LEVELS_KEY = 'levels'
var STORAGE_PLAYED_COUNT_KEY = 'played_count'

var SCENE_MAIN = 'main'
var SCENE_LEVEL = 'level'

var PN_TAG_NEW_LEVEL = 1
var PN_TAG_NEXT_LEVEL = 2
var PN_TAG_REMINDER = 3

var SIGNAL_CHALLENGE_RESOLVED = 'signal_challenge_resolved'
var SIGNAL_CHALLENGE_STOPPED = 'signal_challenge_stopped'
var SIGNAL_ANSWER_CHECKED = 'signal_answer_checked'
var SIGNAL_ANSWER_EXPLODED = 'signal_answer_exploded'
