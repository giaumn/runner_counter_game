extends RigidBody

var _value
var _correct_value

func _init():
	add_user_signal(Constant.SIGNAL_ANSWER_CHECKED)

func init(value, correct_value):
	_value = value
	_correct_value = correct_value
	$Viewport/Label.text = var2str(_value)

func _on_Answer_body_entered(_box):
	var correct = _correct_value == _value
	if correct:
		# correct, show correct animation
		$CollisionShape.disabled = true
		$MeshInstance.hide()
		$Sprite3D.hide()
		$Explosion.restart()
		$Explosion.show()
		$Explosion.one_shot = true
		Sound.play('click.wav')
	else:
		Sound.play('break.wav')
	emit_signal(Constant.SIGNAL_ANSWER_CHECKED, _value, _correct_value)
