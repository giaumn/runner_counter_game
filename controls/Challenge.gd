extends Spatial

var _rng = RandomNumberGenerator.new()
var _answers = []
var false_answers = []
var _correct_answer
var _obstacles = []
var _fibo_level

var target_value

func _init():
	add_user_signal(Constant.SIGNAL_ANSWER_EXPLODED)
	add_user_signal(Constant.SIGNAL_CHALLENGE_RESOLVED)
	add_user_signal(Constant.SIGNAL_CHALLENGE_STOPPED)
	_fibo_level = 4
	_rng.randomize()

func _ready():
	_answers.append($Answer1)
	_answers.append($Answer2)
	_answers.append($Answer3)
	_obstacles.append([
		$Obstacle1,
		$Obstacle2,
		$Obstacle3
	])
	_obstacles.append([
		$Obstacle4,
		$Obstacle5,
		$Obstacle6
	])
	
func _set_fibo_level(initital_value):
	if initital_value < 50:
		_fibo_level = 4
	elif initital_value < 100:
		_fibo_level = 5
	elif initital_value < 200:
		_fibo_level = 6
	elif initital_value < 500:
		_fibo_level = 7
	elif initital_value < 800:
		_fibo_level = 8
	elif initital_value < 1000:
		_fibo_level = 9
	elif initital_value < 2000:
		_fibo_level = 10
	elif initital_value < 3000:
		_fibo_level = 11
	elif initital_value < 5000:
		_fibo_level = 12
	elif initital_value < 8000:
		_fibo_level = 13
	elif initital_value < 10000:
		_fibo_level = 14
	elif initital_value < 20000:
		_fibo_level = 15
	elif initital_value < 30000:
		_fibo_level = 16
	elif initital_value < 50000:
		_fibo_level = 17
	elif initital_value < 80000:
		_fibo_level = 18
	else:
		_fibo_level = 19
	
func init(initital_value, display = null, value = null):
	_set_fibo_level(initital_value)
	
	if display == null:
		var random_change = _rng.randi_range(Fibo.numbers[_fibo_level - 1], Fibo.numbers[_fibo_level])
		target_value = initital_value + random_change
		$Number/Viewport/Label.text = '+' + var2str(random_change)
	else:
		var target_expression = null
		if display.findn('^') > -1:
			target_expression = value.replacen('base', var2str(initital_value))
		else:
			target_expression = var2str(initital_value) + value

		var expression = Expression.new()
		expression.parse(target_expression)
		target_value = int(expression.execute())
		$Number/Viewport/Label.text = display
	
	var other_values = []
	var value_1 = initital_value + _rng.randi_range(Fibo.numbers[_fibo_level - 1], Fibo.numbers[_fibo_level])
	var value_2 = initital_value + _rng.randi_range(Fibo.numbers[_fibo_level - 1], Fibo.numbers[_fibo_level])
	
	while value_1 == target_value:
		value_1 = initital_value + _rng.randi_range(Fibo.numbers[_fibo_level - 1], Fibo.numbers[_fibo_level])
		
	while value_2 == value_1 || value_2 == target_value:
		value_2 = initital_value + _rng.randi_range(Fibo.numbers[_fibo_level - 1], Fibo.numbers[_fibo_level])
	
	if _rng.randi_range(0, 1000) % 3 == 0:
		# randomly make value_2 ends with the same digit as the correct answer
		var correct_number_as_string = String(target_value)
		var last_digit = correct_number_as_string.right(correct_number_as_string.length() - 1)
		var value_2_as_string = String(value_2)
		var new_value_2 = value_2_as_string.left(value_2_as_string.length() - 1) + last_digit
		value_2 = int(new_value_2)

	if value_2 == target_value:
		value_2 += 10
		
	other_values.append(value_1)
	other_values.append(value_2)
	
	var correct_label_index = _rng.randi_range(0, 2)
	_answers[correct_label_index].init(target_value, target_value)
	_correct_answer = _answers[correct_label_index]
	var other_index = 0
	for i in _answers.size():
		if i == correct_label_index:
			continue
		false_answers.append(_answers[i])
		_answers[i].init(other_values[other_index], target_value)
		other_index += 1
		
	for answer in _answers:
		answer.connect(Constant.SIGNAL_ANSWER_CHECKED, self, '_on_answer_checked')

func _on_answer_checked(value, _target_value):
	emit_signal(Constant.SIGNAL_CHALLENGE_RESOLVED, value, _target_value)
	if value == _target_value:
		for false_answer in false_answers:
			false_answer.hide()
		yield(get_tree().create_timer(1), 'timeout')
		emit_signal(Constant.SIGNAL_ANSWER_EXPLODED, self)

func show_obstacles():
	var obstacle_count = _rng.randi_range(0, 1000) % 3 # maximum 2 obstacles
	for i in obstacle_count:
		var obstacle_index = _rng.randi_range(0, 2)
		_obstacles[i][obstacle_index].show()
		_obstacles[i][obstacle_index].get_node('CollisionShape').disabled = false

# warning-ignore:unused_argument
func _on_Obstacle_body_entered(body):
	Sound.play('break.wav')
	emit_signal(Constant.SIGNAL_CHALLENGE_STOPPED)
