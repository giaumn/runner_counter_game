extends RigidBody

export(int) var value setget set_value, get_value

func _ready():
	value = 0

func set_value(new_value):
	value = new_value
	$MeshInstance/Viewport/Label.text = var2str(value)
	
func get_value():
	return value
