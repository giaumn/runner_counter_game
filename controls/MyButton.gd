extends TextureButton

class_name MyButton

export(String) var click_sound
export(bool) var analytic
export(String) var analytic_event_name

func _ready():
	# warning-ignore:return_value_discarded
	self.connect('pressed', self, '_on_pressed')

func _on_pressed():
	Sound.play(self.click_sound)
	if self.analytic:
		self.analytic_event_name = self.name if self.analytic_event_name == null || self.analytic_event_name == '' else self.analytic_event_name
		print(self.analytic_event_name)
		# FBA.logEvent(self.analytic_event_name)
