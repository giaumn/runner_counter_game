extends Control

var is_vibrate_enabled = true

func _ready():
	var sound_enabled = Storage.get_item(Constant.STORAGE_SOUND_KEY)
	sound_enabled = true if sound_enabled == null else sound_enabled
	var vibrate_enabled = Storage.get_item(Constant.STORAGE_VIBRATE_KEY)
	vibrate_enabled = true if vibrate_enabled == null else vibrate_enabled
	$Sound.pressed = bool(sound_enabled)
	$Vibrate.pressed = bool(vibrate_enabled)
	_set_sound($Sound.pressed)
	_set_vibrate($Vibrate.pressed)
	
func _set_sound(enabled):
	Storage.set_item(Constant.STORAGE_SOUND_KEY, enabled)
	if $Sound.pressed:
		Sound.change_volume(Constant.SOUND_VOLUME_DEFAULT)
	else:
		Sound.change_volume(Constant.SOUND_VOLUME_MIN)
		
func _set_vibrate(enabled):
	Storage.set_item(Constant.STORAGE_VIBRATE_KEY, enabled)
	is_vibrate_enabled = enabled

func _on_Sound_button_up():
	_set_sound($Sound.pressed)

func _on_Vibrate_button_up():
	_set_vibrate($Vibrate.pressed)
