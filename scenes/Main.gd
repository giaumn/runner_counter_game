extends Spatial

var Challenge = preload('res://controls/Challenge.tscn')
var _challenge_count = 0
var _latest_challenge
var _started = false
var _challenges = []
var _played_count = 0
var _is_show_ad = false

func _ready():
	var scene = Storage.get_item(Constant.STORAGE_SCENE_KEY)
	if scene == Constant.SCENE_LEVEL:
		# warning-ignore:return_value_discarded
		get_tree().change_scene('res://scenes/Level.tscn')
		return
	_add_challenge()
	_add_challenge()
	_add_challenge()
	_add_challenge()
	
	if Levels.has_more():
		$Level/Label/New.show()
		$Level/Label.text = 'NEW LEVELS'
	else: 
		$Level/Label/New.hide()
		$Level/Label.text = 'LEVELS'
		
	var high_score = Storage.get_item(Constant.STORAGE_HIGH_SCORE_KEY)
	high_score = 0 if high_score == null else high_score
	$HighScore.text = var2str(high_score)
		
	if OS.get_name() == Constant.OS_IOS && !LPN.is_inited():
		LPN.init()
		
	LPN.cancel(Constant.PN_TAG_REMINDER)
	LPN.show('Get your head straight with these numbers! ❤', 'Feeling stumped?', 86400, Constant.PN_TAG_REMINDER, 86400 * 2)
	
	# warning-ignore:return_value_discarded
	MobileAds.connect('initialization_complete', self, '_on_admob_inited')
	# warning-ignore:return_value_discarded
	MobileAds.connect('banner_failed_to_load', self, '_on_AdMob_banner_failed_to_load')
	# warning-ignore:return_value_discarded
	MobileAds.connect('user_earned_rewarded', self, '_on_AdMob_rewarded')
	# warning-ignore:return_value_discarded
	MobileAds.connect('rewarded_ad_failed_to_load', self, '_on_AdMob_rewarded_video_failed_to_load')
	# warning-ignore:return_value_discarded
	MobileAds.connect('interstitial_failed_to_load', self, '_on_AdMob_interstitial_failed_to_load')
	# warning-ignore:return_value_discarded
	MobileAds.connect('interstitial_closed', self, '_on_AdMob_interstitial_closed')
	MobileAds.request_user_consent()
	MobileAds.load_banner()
	MobileAds.load_interstitial()
	MobileAds.load_rewarded()

func _on_admob_inited(status, adapter):
	print('Admob status: ', status, '-', adapter)

func _process(delta):
	if !_started:
		return
	$Lane.transform.origin.z += delta * 8
	
func _unhandled_input(event: InputEvent) -> void:
	var valid_event: bool = (
		event is InputEventSwipe
	)
	
	if not valid_event:
		return

	if event.direction.x == 1:
		_move_box_left()
	elif event.direction.x == -1:
		_move_box_right()
	get_tree().set_input_as_handled()
	
func _move_box_left():
	if !_started or $Lane/Box.transform.origin.x >= 4:
		return
	$Lane/Box.transform.origin.x += 4.1
	
func _move_box_right():
	if !_started or $Lane/Box.transform.origin.x < -4:
		return
	$Lane/Box.transform.origin.x -= 4.1
	
func _add_challenge():
	var current_value = $Lane/Box.get_value()
	if _latest_challenge != null:
		current_value = _latest_challenge.target_value
	
	var challenge = Challenge.instance()
	add_child(challenge)
	challenge.transform.origin.z = 6 + 24 * _challenge_count
	challenge.init(current_value)
	challenge.connect(Constant.SIGNAL_CHALLENGE_RESOLVED, self, '_on_challenge_resolved')
	challenge.connect(Constant.SIGNAL_CHALLENGE_STOPPED, self, '_on_challenge_stopped')
	challenge.connect(Constant.SIGNAL_ANSWER_EXPLODED, self, '_on_answer_exploded')
	_challenge_count += 1
	_latest_challenge = challenge
	_challenges.append(challenge)

func _game_over():
	# incorrect, show game over
	_started = false
	$Lane/Box/MeshInstance.hide()
	$Lane/Box/Explosion.restart()
	$Lane/Box/Explosion.show()
	$Lane/Box/Explosion.one_shot = true
	$GameOver.show()
	
	var score = int($Lane/Box.get_value())
	var high_score = Storage.get_item(Constant.STORAGE_HIGH_SCORE_KEY)
	high_score = 0 if high_score == null else int(high_score)
	if score > high_score:
		Storage.set_item(Constant.STORAGE_HIGH_SCORE_KEY, score)
		$HighScore.text = var2str(score)
	else:
		$HighScore.text = var2str(high_score)

func _restart_game():
	$Start.show()
	$GameOver.hide()
	$Lane/Box/MeshInstance.show()
	$Lane/Box/Explosion.hide()
	$Lane/Box/MeshInstance/Viewport/Label.text = 'START'
	$Lane/Box.set_value(0)
	$Lane/Box.transform.origin.x = 0
	$Lane.transform.origin.z = 0
	$Score/Label.text = '0'
	$Level.show()
	for challenge in _challenges:
		remove_child(challenge)
		challenge.queue_free()
	_challenges = []
	_challenge_count = 0
	_latest_challenge = null
	_add_challenge()
	_add_challenge()
	_add_challenge()
	_add_challenge()

func _on_challenge_resolved(selected, correct_value):
	if $Shared.is_vibrate_enabled:
		Input.vibrate_handheld()
		
	if selected == correct_value:
		# correct, generate a new challenge
		$Lane/Box.set_value(correct_value)
		$Score/Label.text = var2str(correct_value)
		_challenges.pop_front()
		_add_challenge()
		var next_challenge = _challenges[0]
		next_challenge.show_obstacles()
	else:
		_game_over()

func _on_challenge_stopped(): 
	_game_over()
	
func _on_answer_exploded(challenge):
	challenge.queue_free()
	
func _on_Start_button_up():
	FBA.logEvent('start')
	$Start.hide()
	$Score.show()
	$Lane/Box/MeshInstance/Viewport/Label.text = '0'
	$Level.hide()
	_played_count += 1
	yield(get_tree().create_timer(1), 'timeout')
	_started = true

func _on_Restart_button_up():	
	if _is_show_ad:
		return
	
	var all_count = Storage.get_item(Constant.STORAGE_PLAYED_COUNT_KEY)
	all_count = 0 if all_count == null else all_count
	all_count = all_count + 1
	Storage.set_item(Constant.STORAGE_PLAYED_COUNT_KEY, all_count)
	
	if all_count > 3 && Fibo.numbers.find(all_count) > -1:
		print('show rate me')
		RateMe.show()
	
	FBA.logEvent('restart')
	_started = false
	if _played_count > 0 && _played_count % 3 == 0 && MobileAds.get_is_interstitial_loaded():
		_is_show_ad = true
		MobileAds.show_interstitial()
	else:
		MobileAds.load_interstitial()
		_restart_game()

func _on_Continue_button_up():
	if _is_show_ad:
		return
	
	FBA.logEvent('continue')
	
	if MobileAds.get_is_rewarded_loaded():
		_is_show_ad = true
		MobileAds.show_rewarded()
	else:
		MobileAds.load_rewarded()

func _on_AdMob_banner_failed_to_load(error_code):
	print('banner error: ', error_code)
	# reload after 5 seconds
	yield(get_tree().create_timer(5), 'timeout')
	MobileAds.load_banner()

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_AdMob_rewarded(currency, ammount):
	yield(get_tree().create_timer(0.5), 'timeout')
	MobileAds.load_rewarded()
	$Lane/Box/MeshInstance.show()
	$Lane/Box/Explosion.hide()
	$Lane.transform.origin.z = $Lane.transform.origin.z - 20
	$Lane/Box.transform.origin.x = 0
	$GameOver.hide()
	yield(get_tree().create_timer(3), 'timeout')
	_started = true
	_is_show_ad = false

func _on_AdMob_rewarded_video_failed_to_load(error_code):
	print('rewarded video error: ', error_code)
	_is_show_ad = false

func _on_AdMob_interstitial_failed_to_load(error_code):
	print('interstitial error: ', error_code)
	_is_show_ad = false

func _on_AdMob_interstitial_closed():
	yield(get_tree().create_timer(0.5), 'timeout')
	_restart_game()
	_is_show_ad = false

func _on_Level_button_up():
	Storage.set_item(Constant.STORAGE_SCENE_KEY, Constant.SCENE_LEVEL)
	# warning-ignore:return_value_discarded
	get_tree().change_scene('res://scenes/Level.tscn')
