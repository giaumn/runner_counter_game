extends Spatial

var Challenge = preload('res://controls/Challenge.tscn')
var _challenge_count = 0
var _challenges = []
var _latest_challenge
var _started = false
var _played_count = 0
var _is_show_ad = false
var _level = null
var _next_level = null

func _init():
	_load_level()
	
func _ready():
	_init_level()
	
	# warning-ignore:return_value_discarded
	MobileAds.connect('initialization_complete', self, '_on_admob_inited')
	# warning-ignore:return_value_discarded
	MobileAds.connect('banner_failed_to_load', self, '_on_AdMob_banner_failed_to_load')
	# warning-ignore:return_value_discarded
	MobileAds.connect('user_earned_rewarded', self, '_on_AdMob_rewarded')
	# warning-ignore:return_value_discarded
	MobileAds.connect('rewarded_ad_failed_to_load', self, '_on_AdMob_rewarded_video_failed_to_load')
	# warning-ignore:return_value_discarded
	MobileAds.connect('interstitial_failed_to_load', self, '_on_AdMob_interstitial_failed_to_load')
	# warning-ignore:return_value_discarded
	MobileAds.connect('interstitial_closed', self, '_on_AdMob_interstitial_closed')
	MobileAds.load_banner()
	MobileAds.load_interstitial()
	MobileAds.load_rewarded()

func _on_admob_inited(status, adapter):
	print('Admob status: ', status, '-', adapter)
	
func _load_level():
	var current_level = Storage.get_item(Constant.STORAGE_CURRENT_LEVEL_KEY)
	current_level = 0 if current_level == null else current_level
	_level = Levels.get_level(current_level)
	if _level == null:
		return
	_next_level = Levels.get_level(current_level + 1)
	
func _init_level():
	if _level == null:
		# no more levels to play
		$GameOver.show()
		$GameOver/Status.text = 'Well done!'
		$GameOver/Status/Label.text = 'You completed all levels!'
		$GameOver/Next.hide()
		$GameOver/Continue.hide()
		$GameOver/Restart.hide()
		$GameOver/Reset.show()
		$Start.hide()
		$Lane/Box/MeshInstance.hide()
		$Update.show()
		$Return.show()
		$Name.hide()
	else:
		$Name.show()
		$Name/Label.text = _level.get('name')
		FBA.logEvent(_level.get('name'))
		var level_initial_value = int(_level.get('initial'))
		$Lane/Box/MeshInstance/Viewport/Label.text = var2str(level_initial_value)
		$Lane/Box.set_value(level_initial_value)
		for step in _level.get('steps'):
			_add_step(step.get('display'), step.get('value'))
		
func _add_step(display, value):
	var current_value = $Lane/Box.get_value()
	var challenge = Challenge.instance()
	add_child(challenge)
	
	if _latest_challenge != null:
		current_value = _latest_challenge.target_value
		
	challenge.transform.origin.z = 6 + 24 * _challenge_count
	challenge.init(int(current_value), display, value)
	challenge.connect(Constant.SIGNAL_CHALLENGE_RESOLVED, self, '_on_challenge_resolved')
	challenge.connect(Constant.SIGNAL_CHALLENGE_STOPPED, self, '_on_challenge_stopped')
	challenge.connect(Constant.SIGNAL_ANSWER_EXPLODED, self, '_on_answer_exploded')
	
	if _challenge_count == 0:
		challenge.show_obstacles()
		
	_challenge_count += 1
	_latest_challenge = challenge
	_challenges.append(challenge)

func _game_over():
	# incorrect, show game over
	_started = false
	$Lane/Box/MeshInstance.hide()
	$Lane/Box/Explosion.restart()
	$Lane/Box/Explosion.show()
	$Lane/Box/Explosion.one_shot = true
	$GameOver.show()
	if _challenges.size() == 0:
		# level completed
		$GameOver/Status.text = _level.get('name')
		$GameOver/Status/Label.text = 'Completed'
		var next_level = null
		LPN.cancel(Constant.PN_TAG_NEXT_LEVEL)
		if _next_level == null:
			next_level = int(_level.get('level') + 1)
			Storage.set_item(Constant.STORAGE_CURRENT_LEVEL_KEY, next_level)
			$GameOver/Status.text = 'Well done!'
			$GameOver/Status/Label.text = 'You completed all levels!'
			$GameOver/Next.hide()
			$GameOver/Continue.hide()
			$GameOver/Restart.hide()
			$GameOver/Reset.show()
			$Return.show()
		else:
			next_level = int(_next_level.get('level'))
			LPN.show('Wanna try again?  ', _next_level.get('name') + ' is incomplete', 7200, Constant.PN_TAG_NEXT_LEVEL, 86400)
			Storage.set_item(Constant.STORAGE_CURRENT_LEVEL_KEY, next_level)
			$GameOver/Next.show()
			$GameOver/Continue.hide()
			$GameOver/Restart.show()
			$GameOver/Reset.hide()
			$GameOver/Next/Label.text = _next_level.get('name')
		
		if !Levels.has_more():
			LPN.cancel(Constant.PN_TAG_NEW_LEVEL)
	else:
		# level crashed
		$GameOver/Status.text = _level.get('name')
		$GameOver/Status/Label.text = 'Crashed. Try again!'
		$GameOver/Next.hide()
		$GameOver/Continue.show()
		$GameOver/Restart.show()
		$GameOver/Reset.hide()

func _restart_game():
	$Start.show()
	$Return.show()
	$GameOver.hide()
	$Lane/Box/MeshInstance.show()
	$Lane/Box/Explosion.hide()
	$Lane/Box/MeshInstance/Viewport/Label.text = 'START'
	$Lane/Box.set_value(0)
	$Lane/Box.transform.origin.x = 0
	$Lane.transform.origin.z = 0
	$Update.hide()
	for challenge in _challenges:
		remove_child(challenge)
		challenge.queue_free()
	_challenges = []
	_challenge_count = 0
	_latest_challenge = null
	_init_level()
		
func _on_challenge_resolved(selected, correct_value):
	if $Shared.is_vibrate_enabled && (OS.get_name() == Constant.OS_IOS || OS.get_name() == Constant.OS_ANDROID):
		Input.vibrate_handheld()
		
	if selected == correct_value:
		# correct, generate a new challenge
		$Lane/Box.set_value(correct_value)
		_challenges.pop_front()
		if _challenges.size() > 0:
			var next_challenge = _challenges[0]
			next_challenge.show_obstacles()
		else:
			_game_over()
	else:
		_game_over()

func _on_challenge_stopped(): 
	_game_over()
	
func _on_answer_exploded(challenge):
	challenge.queue_free()

func _process(delta):
	if !_started:
		return
	$Lane.transform.origin.z += delta * 10
	
func _unhandled_input(event: InputEvent) -> void:
	var valid_event: bool = (
		event is InputEventSwipe
	)
	
	if not valid_event:
		return

	if event.direction.x == 1:
		_move_box_left()
	elif event.direction.x == -1:
		_move_box_right()
	get_tree().set_input_as_handled()
	
func _move_box_left():
	if !_started or $Lane/Box.transform.origin.x >= 4:
		return
	$Lane/Box.transform.origin.x += 4.1
	
func _move_box_right():
	if !_started or $Lane/Box.transform.origin.x < -4:
		return
	$Lane/Box.transform.origin.x -= 4.1

func _on_Start_button_up():
	FBA.logEvent('start')
	$Start.hide()
	$Name.show()
	$Return.hide()
	_played_count += 1
	yield(get_tree().create_timer(1), 'timeout')
	_started = true

func _on_Restart_button_up():
	if _is_show_ad:
		return
	
	var all_count = Storage.get_item(Constant.STORAGE_PLAYED_COUNT_KEY)
	all_count = 0 if all_count == null else all_count
	all_count = all_count + 1
	Storage.set_item(Constant.STORAGE_PLAYED_COUNT_KEY, all_count)
	
	if all_count > 3 && Fibo.numbers.find(all_count) > -1:
		print('show rate me')
		RateMe.show()
		
	FBA.logEvent('restart')
	_started = false
	if _played_count > 0 && _played_count % 3 == 0 && MobileAds.get_is_interstitial_loaded():
		_is_show_ad = true
		MobileAds.show_interstitial()
	else:
		MobileAds.load_interstitial()
		_restart_game()

func _on_Continue_button_up():
	if _is_show_ad:
		return
	
	FBA.logEvent('continue')
	
	if MobileAds.get_is_rewarded_loaded():
		_is_show_ad = true
		MobileAds.show_rewarded()
	else:
		MobileAds.load_rewarded()

func _on_AdMob_banner_failed_to_load(error_code):
	print('banner error: ', error_code)
	# reload after 5 seconds
	yield(get_tree().create_timer(5), 'timeout')
	MobileAds.load_banner()

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_AdMob_rewarded(currency, ammount):
	yield(get_tree().create_timer(0.5), 'timeout')
	MobileAds.load_rewarded()
	$Lane/Box/MeshInstance.show()
	$Lane/Box/Explosion.hide()
	$Lane.transform.origin.z = $Lane.transform.origin.z - 20
	$Lane/Box.transform.origin.x = 0
	$GameOver.hide()
	yield(get_tree().create_timer(3), 'timeout')
	_started = true
	_is_show_ad = false

func _on_AdMob_rewarded_video_failed_to_load(error_code):
	print('rewarded video error: ', error_code)
	_is_show_ad = false

func _on_AdMob_interstitial_failed_to_load(error_code):
	print('interstitial error: ', error_code)
	_is_show_ad = false

func _on_AdMob_interstitial_closed():
	yield(get_tree().create_timer(0.5), 'timeout')
	_restart_game()
	_is_show_ad = false

func _on_Return_button_up():
	Storage.set_item(Constant.STORAGE_SCENE_KEY, Constant.SCENE_MAIN)
	# warning-ignore:return_value_discarded
	get_tree().change_scene('res://scenes/Main.tscn')

func _on_Next_button_up():
	Storage.set_item(Constant.STORAGE_CURRENT_LEVEL_KEY, int(_next_level.get('level')))
	_load_level()
	_restart_game()

func _on_Reset_button_up():
	Storage.set_item(Constant.STORAGE_CURRENT_LEVEL_KEY, null)
	_load_level()
	_restart_game()
